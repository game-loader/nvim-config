local pluginKeys = {}

-- 设定映射函数
-- set map function
local map = vim.api.nvim_set_keymap
local opt = { noremap = true, silent = true }
local wk = require("which-key")

-- basic operation for write and quit
-- 文件写入退出基本操作
wk.add({
	{ "<Leader>s", ":w!<CR>", desc = "Save File" },
	{ "<Leader>q", ":qa<CR>", desc = "Quit All" },
	{ "<Leader>S", ":wa<CR>", desc = "Save All" },
})

-- iron related
wk.add({
	{ "<leader>i", group = "Iron" },
	{ "<leader>il", "<Cmd>lua require('iron.core').send_line()<CR>", desc = "Send line" },
	{ "<leader>if", "<Cmd>lua require('iron.core').send_file()<CR>", desc = "Send file" },
	{ "<leader>ic", "<Cmd>lua require('iron.core').send_until_cursor()<CR>", desc = "Send to cursor" },
})

-- lsp 回调快捷键设置
pluginKeys.maplsp = function(mapbuf)
	-- go xx
	mapbuf("n", "]d", "<cmd>lua vim.lsp.buf.definition()<CR>", opt)
	-- mapbuf("n", "gh", "<cmd>lua vim.lsp.buf.hover()<CR>", opt)
	mapbuf("n", "]D", "<cmd>lua vim.lsp.buf.declaration()<CR>", opt)
	mapbuf("n", "]i", "<cmd>lua vim.lsp.buf.implementation()<CR>", opt)
	mapbuf("n", "]r", "<cmd>lua vim.lsp.buf.references()<CR>", opt)
	-- diagnostic
	mapbuf("n", "]p", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opt)
	mapbuf("n", "]n", "<cmd>lua vim.diagnostic.goto_next()<CR>", opt)
	mapbuf("n", "]a", "<cmd>lua vim.lsp.buf.code_action()<CR>", opt)
	mapbuf("n", "]h", "<cmd>lua vim.lsp.buf.hover()<CR>", opt)
end

-- code related
-- 代码相关
map("n", ",", ":Jaq<CR>", opt)

-- markdown related
wk.add({
	{ "<Leader>m", ":RenderMarkdown toggle<CR>", desc = "Markdown preview" },
})

-- git related
wk.add({
	{ "<Leader>g", ":LazyGit<CR>", desc = "Open lazygit" },
})

-- file related
wk.add({
	{ "<Leader><Tab>", "<C-^>", desc = "Last file" },
})

wk.add({
	{ "<Leader>f", group = "File" },
	{ "<Leader>fp", ":Telescope projects<CR>", desc = "Open project" },
	{ "<Leader>fr", ":Telescope oldfiles<CR>", desc = "Recent files" },
	{ "<Leader>fb", ":Telescope file_browser<CR>", desc = "File browser" },
	{ "<Leader>fn", ":AdvancedNewFile<CR>", desc = "New file" },
	{ "<Leader>fs", ":Telescope live_grep<CR>", desc = "Search in project" },
	{ "<Leader>ff", ":Telescope find_files<CR>", desc = "Search file" },
	{ "<Leader>fc", ":source $MYVIMRC<CR>", desc = "Reload config file" },
})

-- jk map to esc
-- jk映射为esc键
map("i", "jk", "<Esc>", opt)

-- window operate by which-key
wk.add({
	{ "<Leader>w", group = "Window" },
	{ "<Leader>wh", "<C-w>h", desc = "To left" },
	{ "<Leader>wj", "<C-w>j", desc = "To up" },
	{ "<Leader>wk", "<C-w>k", desc = "To down" },
	{ "<Leader>wl", "<C-w>l", desc = "To right" },
	{ "<Leader>ws", ":sp<CR>", desc = "Split window" },
	{ "<Leader>wv", ":vsplit<CR>", desc = "Vsplit window" },
	{ "<Leader>wd", ":close<CR>", desc = "Close window" },
	{ "<Leader>wo", ":only<CR>", desc = "Close others" },
})

-- base operation for visual mode
-- 可视模式下基本操作
map("v", "<", "<gv", opt)
map("v", ">", ">gv", opt)

-- neoTree
map("n", "T", ":NeoTreeFocusToggle<CR>", opt)

-- 定义开关quickfix窗口的函数

local function toggle_quickfix()
	-- 获取所有窗口的信息
	local wininfo = vim.fn.getwininfo()
	-- 遍历所有窗口
	for _, win in pairs(wininfo) do
		-- 如果这个窗口是 quickfix 窗口
		if win.quickfix == 1 then
			-- 关闭 quickfix 窗口
			vim.cmd("cclose")
			-- 返回，不再继续查找
			return
		end
	end
	-- 如果没有找到 quickfix 窗口，就打开一个
	vim.cmd("copen")
end

-- Bufferline and buffer related
wk.add({
	{ "<Leader>b", group = "Buffer" },
	{ "<Leader>bk", ":bd!<CR>", desc = "Kill buffer" },
	{ "<Leader>bo", ":BufferLineCloseRight<CR>:BufferLineCloseLeft<CR>", desc = "Close other buffer" },
	{ "<Leader>bb", ":Telescope buffers<CR>", desc = "Open buffers" },
	{ "<Leader>bn", ":ls<CR>", desc = "Buffer numbers" },
	{ "<Leader>bc", ":noh<CR>", desc = "Cancel highlight" },
	{ "<Leader>bC", ":call setqflist([], 'r')<CR>", desc = "Clear quickfix" },
	{
		"<Leader>bq",
		function()
			toggle_quickfix()
		end,
		desc = "Toggle quickfix",
	},
	{ "<Leader>bs", ":Telescope current_buffer_fuzzy_find<CR>", desc = "Searching in buffer" },
})

wk.add({
	{ "<Leader>j", ":HopLineStart<CR>", desc = "Quick jump line" },
})

wk.add({
	{ "<Leader>c", group = "GPT" },
	{ "<Leader>ct", ":CodeCompanionChat Toggle<CR>", desc = "Code companion chat" },
	{
		mode = { "n", "v" }, -- NORMAL and VISUAL mode
		{ "<leader>ca", "<cmd>CodeCompanionActions<cr>", desc = "Code companion actions" },
	},
})

-- 快速切换主题
-- wk.register({
-- 	["<Leader>c"] = { ":Telescope colorscheme<CR>", "Quick change colortheme" },
-- })

map("n", "<Tab>", "za", opt)
-- insert 模式下ctrl a e跳转开头结尾
map("i", "<C-a>", "<C-o>I", opt)
map("i", "<C-e>", "<C-o>A", opt)

-- change left and right tab
-- 左右Tab切换
map("n", "<C-h>", ":BufferLineCyclePrev<CR>", opt)
map("n", "<C-l>", ":BufferLineCycleNext<CR>", opt)
map("i", "<C-h>", "<C-o>:BufferLineCyclePrev<CR>", opt)
map("i", "<C-l>", "<C-o>:BufferLineCycleNext<CR>", opt)

-- Mason
wk.add({
	{ "<Leader>l", group = "Lsp" },
	{ "<Leader>li", ":LspInstall<CR>", desc = "Install lsp" },
	{ "<Leader>lI", ":MasonInstall ", desc = "Install any" },
	{ "<Leader>lr", ":LspRestart<CR>", desc = "Lsp restart" },
	{ "<Leader>lm", ":Mason<CR>", desc = "Mason info" },
	{ "<Leader>lu", ":MasonUninstall<CR>", desc = "Uninstall lsp" },
	{ "<Leader>lU", ":MasonUninstallAll<CR>", desc = "Unistall all" },
	{ "<Leader>ll", ":LspInfo<CR>", desc = "Lsp infos" },
	{ "<Leader>lR", vim.lsp.buf.rename, desc = "Buffer var rename" },
})

-- dap keymaps
wk.add({
	{ "<Leader>d", group = "Debug" },
	{ "<Leader>dr", ":lua require('dap').continue()<CR>", desc = "Start debug" },
	{ "<Leader>db", ":lua require('dap').toggle_breakpoint()<CR>", desc = "Set breakpoint" },
	{ "<Leader>dc", ":lua require('dap').clear_breakpoints()<CR>", desc = "Clear breakpoint" },
	{
		"<Leader>de",
		":lua require'dap'.close()<CR>:lua require'dap'.terminate()<CR>:lua require'dap.repl'.close()<CR>:lua require'dapui'.close()<CR>:DapVirtualTextDisable<CR><C-w>o<CR>",
		desc = "Stop debug",
	},
})

-- cmpeletion keys
-- 补全快捷键
pluginKeys.cmp = function(cmp)
	return {
		-- next option
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif require("luasnip").expand_or_jumpable() then
				require("luasnip").expand_or_jump()
			else
				fallback()
			end
		end, { "i", "s" }), -- 下一个

		["<Up>"] = cmp.mapping.select_prev_item(),

		["<CR>"] = cmp.mapping.confirm({
			select = true,
			behavior = cmp.ConfirmBehavior.Replace,
		}),
	}
end

map("i", "<C-d>", ":lua require'dap'.continue()<CR>", opt)
map("n", "<C-n>", ":lua require'dap'.step_into()<CR>", opt)
map("n", "<C-o>", ":lua require'dap'.step_over()<CR>", opt)

-- set keymap based on file type
vim.cmd("autocmd FileType * lua SetKeybinds()")

function SetKeybinds()
	local fileTy = vim.api.nvim_buf_get_option(0, "filetype")
	local opts = { prefix = "<localleader>", buffer = 0 }

	if fileTy == "markdown" then
		wk.add({
			{ "<localleader>t", ":InsertNToc<CR>", desc = "Insert table of content", buffer = 0 },
			{ "<localleader>d", ":HeaderDecrease<CR>", desc = "All header decrease", buffer = 0 },
			{ "<localleader>i", ":HeaderIncrease<CR>", desc = "All header increase", buffer = 0 },
		})
	elseif fileTy == "python" then
		wk.add({
			{ "<localleader>r", ":MagmaEvaluateOperator<CR>", desc = "Jupyter evaluate", buffer = 0 },
			{ "<localleader>c", ":MagmaEvaluateCell<CR>", desc = "Jupyter evaluate cell", buffer = 0 },
		})
	elseif fileTy == "dart" then
		wk.add({
			{ "<localleader>r", ":FlutterRun<CR>", desc = "FlutterRun", buffer = 0 },
			{ "<localleader>q", ":FlutterQuit<CR>", desc = "FlutterQuit", buffer = 0 },
			{ "<localleader>c", ":FlutterCopyProfilerUrl<CR>", desc = "FlutterCopyProfilerUrl", buffer = 0 },
			{ "<localleader>w", ":FlutterDevTools<CR>", desc = "FlutterDevTools", buffer = 0 },
		})
		-- elseif fileTy == "sh" then
		--     wk.add({
		--         { "<localleader>W", ":w<CR>", desc = "test write", buffer = 0 },
		--         { "<localleader>Q", ":q<CR>", desc = "test quit", buffer = 0 },
		--     })
	end
end

return pluginKeys
