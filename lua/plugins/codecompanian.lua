return {
	"olimorris/codecompanion.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-treesitter/nvim-treesitter",
		"hrsh7th/nvim-cmp", -- Optional: For using slash commands and variables in the chat buffer
		"nvim-telescope/telescope.nvim", -- Optional: For using slash commands
		{ "stevearc/dressing.nvim", opts = {} }, -- Optional: Improves the default Neovim UI
	},
	config = true,
	opts = {
		log_level = "TRACE",
		strategies = {
			chat = {
				adapter = "openai",
			},
			inline = {
				adapter = "openai",
			},
			agent = {
				adapter = "openai",
			},
		},
		adapters = {
			openai = function()
				return require("codecompanion.adapters").extend("openai", {
					env = {
						api_key = "cmd:sed -n '1p' ~/.config/nvim/secrets/openai_config.txt | tr -d '\n'",
					},
					schema = {
						model = {
							default = "claude-3-5-sonnet-20240620",
						},
					},
					url = "cmd:sed -n '2p' ~/.config/nvim/secrets/openai_config.txt | tr -d '\n'",
					opts = {
						stream = false,
					},
				})
			end,
		},
	},
}
